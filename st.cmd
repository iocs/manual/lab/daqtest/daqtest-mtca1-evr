#- common E3 modules
require busy
require calc
require essioc

#- EVR modules
require mrfioc2

#- ----------------------------------------------------------------------------
#- General Configuration
#- ----------------------------------------------------------------------------
epicsEnvSet("SYSSUB",       "LabS-ICS")
epicsEnvSet("EVR_DEV",      "Ctrl-EVR-411")
epicsEnvSet("IOCDEV",       "SC-IOC-411")

#- ----------------------------------------------------------------------------
#- essioc Configuration
#- ----------------------------------------------------------------------------
epicsEnvSet("IOCNAME",       "$(SYSSUB):$(IOCDEV)")
epicsEnvSet("IOCDIR",        "$(SYSSUB)_$(IOCDEV)")
#epicsEnvSet("AS_TOP",        "/tmp")
iocshLoad("$(essioc_DIR)/common_config.iocsh")

#- ----------------------------------------------------------------------------
#- EVR Configuration
#- ----------------------------------------------------------------------------
epicsEnvSet("PEVR","$(SYSSUB):$(EVR_DEV)")
epicsEnvSet("PCIID","0e:00.0")

#- EVR Setup
mrmEvrSetupPCI "$(OBJ=EVR)" "$(PCIID)"
dbLoadRecords "$(mrfioc2_DB)/evr-mtca-300-univ.db" "P=$(PEVR), EVR=$(OBJ=EVR), FEVT=88.0525"
var evrMrmTimeNSOverflowThreshold 500000000

#- Databuffer database
dbLoadRecords("$(E3_CMD_TOP)/db/evr-databuffer-ess.db","P=$(PEVR)")

#- Events
iocshLoad("$(E3_CMD_TOP)/iocsh/evrevt.iocsh","P=$(PEVR)")

#- After init actions
afterInit("iocshLoad '$(E3_CMD_TOP)/iocsh/evr.r.iocsh' 'P=$(PEVR)'")
afterInit("$(EVRAMC2CLKEN=)iocshLoad '$(E3_CMD_TOP)/iocsh/evrtclk.r.iocsh' 'P=$(PEVR)'")

#- Delay Generators config
afterInit("iocshLoad '$(E3_CMD_TOP)/iocsh/evrdlygen.r.iocsh' 'P=$(PEVR)'")
afterInit("iocshLoad '$(E3_CMD_TOP)/iocsh/evrout.r.iocsh' 'P=$(PEVR)'")

#- ----------------------------------------------------------------------------
#- IOC Start
#- ----------------------------------------------------------------------------
iocInit()

